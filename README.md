

This proof of concept codes, showing the possebility to use a versioning file for tagging instead of the usually used PIPELINE ID Number.
Therefore the build container can clearly be identified and reused in a later stage

#CI Variables

| variable | value (default) | executes |
|----------|:-------------:|------:|
| MANUAL_RELEASE |  true ( false) | do not increase version number, uses the on defined in the version-ci.json file |
| MINOR_RELEASE | true (false) | create new minor version |
| MAJOR_RELEASE | true (false) | create new major version |


#Flow
```mermaid
gitGraph
    commit id: "start"
    branch nice_feature order: 2
    branch prod order: 1
    checkout nice_feature
    checkout main
    checkout nice_feature
    commit
    commit
    commit
    checkout main
    checkout nice_feature
    checkout main
    merge nice_feature id: "merge to main -- linter"
    commit id: "create new image" tag: "0.0.1"
    commit id: "merge request to prod -- trivy scan"
    checkout prod
    merge main
    commit id: "update image tag" tag: "0.0.1"
    checkout main
    branch very_nice_feature order: 3
    checkout very_nice_feature
    commit
    commit
    commit
    checkout main
    merge very_nice_feature id:"merge to main -- linter 2"
    commit id: "create new image 2" tag:"0.0.2"
    commit id: "merge request to prod -- trivy scan 2"
    checkout prod
    merge main
    commit id: "update image tag 2" tag: "0.0.2" 
```
